<?php

$fd = fopen('el_quijote.txt', 'r+');

$numeroMolinos = 0;

while (($contenido = fgets($fd)) !== false) { 
    $contenido = strtolower($contenido);
    $numeroMolinosParrafo = substr_count($contenido, 'molino');

    $numeroMolinos += $numeroMolinosParrafo;

    //echo $contenido . "<br/>";
}

echo $numeroMolinos;

fclose($fd);